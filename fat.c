#include "fat.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

FILE* in;
PartitionTable pt[4];
Fat16BootSector bs;
long fat;
long root;
long current;

int main(int argc, char * argv[]) {
  // input filename
  const char * filename = argc >= 2 ? argv[1] : NULL;

  // init
  in = fopen("sd.img", "rb");
  initFat(filename == NULL);

  // run test sequence
  if (filename != NULL && strcmp(filename, "test") == 0) {
    printf("Test sequence\n\n");
    printf("\n$ cd ADR2\n");
    changeDir("ADR2");
    printf("\n$ cat HISTORIE.TXT\n");
    readFile("HISTORIE.TXT");
    printf("\n$ cd ../ADR1\n");
    changeDir("../ADR1");
    printf("\n$ ls\n");
    listDir();
    printf("\n$ cd /\n");
    changeDir("/");
    printf("\n$ ls\n");
    listDir();
    printf("\n$ cat ABSTRAKT.TXT\n");
    readFile("ABSTRAKT.TXT");
  }
  // filename specified
  else if (filename != NULL) {
    Fat16Entry entry;

    if (locateEntry(filename, &entry) == -1 || entry.attributes == 0x00) {
      fprintf(stderr, "Entry in path %s not found.\n", filename);
      fclose(in);
      return 1;
    }

    // directory
    if ((entry.attributes & 0x10) != 0) {
      if (changeDir(filename)) {
        listDir();
      }
    }
    // file
    else {
      readFile(filename);
    }
  }
  // no argument
  else {
    printTree(0, NULL);
  }

  fclose(in);
  return 0;
}



/*********** Tasks ***********/

void initFat(int verbose) {
  int i;

  // init
  fseek(in, 0x1BE, SEEK_SET);                // go to partition table start, partitions start at offset 0x1BE, see http://www.cse.scu.edu/~tschwarz/coen252_07Fall/Lectures/HDPartitions.html
  fread(pt, sizeof(PartitionTable), 4, in);  // read all entries (4)

  if (verbose) {
    printf("Partition table\n-----------------------\n");
    for (i = 0; i < 4; i++) { // for all partition entries print basic info
      printf("Partition %d, type %02X, ", i, pt[i].partition_type);
      printf("start sector %8d, length %8d sectors\n", pt[i].start_sector, pt[i].length_sectors);
    }
  }

  if (verbose) printf("\nSeeking to first partition by %d sectors\n", pt[0].start_sector);
  fseek(in, 512 * pt[0].start_sector, SEEK_SET); // Boot sector starts here (seek in bytes)
  fread(&bs, sizeof(Fat16BootSector), 1, in);    // Read boot sector content, see http://www.tavi.co.uk/phobos/fat.html#boot_block
  if (verbose) printf("Volume_label %.11s, %d sectors size, %d cluster size\n", bs.volume_label, bs.sector_size, bs.sectors_per_cluster);

  // FAT location
  fseek(in, (bs.reserved_sectors - 1) * bs.sector_size, SEEK_CUR);
  fat = ftell(in);

  // Seek to the beginning of root directory, it's position is fixed
  fseek(in, (bs.fat_size_sectors * bs.number_of_fats) * bs.sector_size, SEEK_CUR);
  current = root = ftell(in);
}

void listDir() {
  int i, batchEntries, isRoot;
  Fat16Entry entry;
  struct tm tm;
  short cluster;
  int files = 0, dirs = 0, totalSize = 0;

  // cursor to current directory
  fseek(in, current, SEEK_SET);
  isRoot = root == current;
  cluster = isRoot ? -1 : currentCluster();
  batchEntries = isRoot ? bs.root_dir_entries : bs.sector_size * bs.sectors_per_cluster / sizeof(Fat16Entry);

  // list directory
  while (1) {
    for (i = 0; i < batchEntries; ++i) {
      fread(&entry, sizeof(entry), 1, in);
      switch (entry.filename[0]) {
        case 0x00:
        case 0xe5:
          continue;
      }
      
      printf("%.8s.%.3s ", entry.filename, entry.ext);
      if (entry.attributes & 0x10) {
        printf(" <DIR>           ");
        ++dirs;
      }
      else if (entry.attributes & 0x08) {
        printf("                 ");
      }
      else {
        printf("        %8d ", entry.file_size);
        ++files;
      }
      totalSize += entry.file_size;
      modifyDateTime(&entry, &tm);
      printf("%04d-%02d-%02d %02d:%02d:%02d", 1900 + tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
      // printf("\tattrs: %x\tcluster: %x", entry.attributes, entry.starting_cluster);
      printf("\n");
    }

    if (isRoot || (cluster = fatLookup(cluster)) == -1) {
      break;
    }
    seekCluster(cluster);
  };
  printf("   %d File(s)         %8d Bytes.\n", files, totalSize);
  printf("   %d Dir(s)          \n", dirs);
}

void printTree(unsigned recursionLevel, const Fat16Entry * parent) {
  Fat16Entry entry;
  int i, j, isRoot = 0, batchEntries;
  short cluster;
  long pos;

  if (recursionLevel == 0) {
    isRoot = 1;
    fseek(in, root, SEEK_SET);
    printf("\n\nDirectory tree\n");
    printf(" / \n");
    cluster = 0x00;
  }
  else {
    if (parent == NULL || (parent->attributes & 0x10) == 0) {
      assert(!"reachable");
    }
    seekCluster(cluster = parent->starting_cluster);
  }

  batchEntries = isRoot ? bs.root_dir_entries : bs.sector_size * bs.sectors_per_cluster / sizeof(Fat16Entry);

  // list directory
  while (1) {
    for (i = 0; i < batchEntries; ++i) {
      fread(&entry, sizeof(entry), 1, in);

      switch (entry.filename[0]) {
        case 0x00:
        case 0xe5:
          continue;
      }

      if ((entry.attributes & 0x10) && !filenameCompare(&entry, ".") && !filenameCompare(&entry, "..")) {
        for (j = 0; j <= recursionLevel; ++j)
          printf("         ");
        printf("%.8s.%.3s\n", entry.filename, entry.ext);

        if (recursionLevel < 5) {
        pos = ftell(in);
        printTree(recursionLevel+1, &entry);
        fseek(in, pos, SEEK_SET);
        }
      }
    }

    if (isRoot || (cluster = fatLookup(cluster)) == -1) {
      break;
    }
    seekCluster(cluster);
  };

  printf("\n");
}

int readFile(const char *filename) {
  int i = 0;
  short cl;
  Fat16Entry entry;

  // locate file
  if (locateEntry(filename, &entry) == -1) {
    fprintf(stderr, "file not found\n");
    return 0;
  }

  // not a directory
  if (entry.attributes != 0x00 && !(entry.attributes & 0x10)) {
    const int bufferSize = bs.sector_size * bs.sectors_per_cluster;
    char buffer[bufferSize];
    unsigned int readLen, remaining = entry.file_size;
    short cluster = entry.starting_cluster;

    while (cluster != -1) {
      seekCluster(cluster);

      readLen = fread(buffer, sizeof(char), bufferSize, in);
      if (readLen <= remaining) {
        remaining -= readLen;
      }
      else {
        readLen = remaining;
        remaining = 0;
      }
      write(1, buffer, readLen);
      
      cluster = fatLookup(cluster);
      if (remaining <= 0 && cluster != -1) {
        assert(!"reachable");
      }
    }
  }
  else return 0;

  return 1;
}

int changeDir(const char *path) {
  Fat16Entry entry;
  if (locateEntry(path, &entry) == -1) {
    fprintf(stderr, "directory not found\n");
    return 0;
  }
  if ((entry.attributes & 0x10) == 0) {
    fprintf(stderr, "not a directory\n");
    return 0;
  }
  current = ftell(in);
  return 1;
}


/*********** Helpers *****************/

void modifyDateTime(Fat16Entry *entry, struct tm *tm) {
  tm->tm_sec = (entry->modify_time & 0x1F) << 1;
  tm->tm_min = (entry->modify_time & (0x3F << 5)) >> 5;
  tm->tm_hour = (entry->modify_time & (0x1F << 11)) >> 11;
  tm->tm_mday = entry->modify_date & 0x1F;
  tm->tm_mon = ((entry->modify_date & (0xF << 5)) >> 5) - 1;
  tm->tm_year = ((entry->modify_date & (0x7F << 9)) >> 9) + (1980 - 1900);
}

int filenameCompare(Fat16Entry *entry, const char *filename) {
  char len, * dot, dotPos, extLen, i;

  // non-existant
  switch (entry->filename[0]) {
    case 0x00:
    case 0xe5:
      return 0;
  }
  
  // special directory names
  if (entry->filename[0] == '.') {
    if (entry->filename[1] == '.') {
      return strcmp(filename, "..") == 0;
    }
    else {
      return strcmp(filename, ".") == 0;
    }
  }

  // filename mapping
  len = strlen(filename);
  dot = strrchr(filename, '.');
  dotPos = dot != NULL ? (dot - filename) : len;
  extLen = len - dotPos - 1;

  // base name or extension limit
  if (dotPos > 8 || extLen > 3) {
    return 0;
  }

  // base name
  for (i = 0; i < 8; ++i) {
    // up to dot, rest must be whitespaced
    if (entry->filename[i] != (i < dotPos ? filename[i] : ' ')) {
      return 0;
    }
  }

  // extension
  for (i = 0; i < 3; ++i) {
    if (entry->ext[i] != (i < extLen ? filename[dotPos + 1 + i] : ' ')) {
      return 0;
    }
  }

  // match
  return 1;
}

short fatLookup(short cluster) {
  short next;
  fseek(in, fat + cluster * sizeof(short), SEEK_SET);
  fread(&next, sizeof(short), 1, in);
  return next;
}

int seekCluster(short cluster) {
  return fseek(in, root + bs.root_dir_entries * sizeof(Fat16Entry) + (cluster - 2) * bs.sectors_per_cluster * bs.sector_size, SEEK_SET);
}

int locateEntry(const char * path, Fat16Entry * entry) {
  fseek(in, current, SEEK_SET);
  return locateEntryRelative(path, entry);
}

int locateEntryRelative(const char * path, Fat16Entry * entry) {
  char len, * slash, partLen, isRoot;

  if ((len = strlen(path)) == 0) {
    return -1;
  }

  slash = strchr(path, '/');
  partLen = slash != NULL ? (slash - path) : len;

  // filename limit exceeded (including .ext)
  if (partLen > 12) {
    fprintf(stderr, "Invalid path - it is too long\n");
    return -1;
  }

  // starts with slash ~ root
  if (partLen == 0) {
    fseek(in, root, SEEK_SET);
    entry->attributes |= 0x10;
    isRoot = 1;
  }
  // TODO implement parent directory `..`
  // find filename in directory
  else {
    char filename[partLen + 1];
    int batchEntries, i = 0;
    short cluster;

    strncpy(filename, path, partLen);
    filename[partLen] = '\0';
    isRoot = root == ftell(in);
    batchEntries = isRoot ? bs.root_dir_entries : bs.sector_size * bs.sectors_per_cluster / sizeof(Fat16Entry);
    cluster = isRoot ? 0x00 : entry->starting_cluster;

    while (1) {
      fread(entry, sizeof(*entry), 1, in);

      if (filenameCompare(entry, filename)) {
        if (entry->starting_cluster == 0x00) {  // root
          fseek(in, root, SEEK_SET);
          entry->attributes |= 0x10;
          isRoot = 1;
          cluster = 0x00;
        }
        else if (entry->starting_cluster > 0x00) {
          seekCluster(entry->starting_cluster);
        }
        else {
          assert(!"reachable");
        }
        break;
      }

      if (++i >= batchEntries) {
        if (isRoot || (cluster = fatLookup(cluster)) < 0) {
          return -1;     // not found
        }
        i = 0;
      }
    }
  }

  // recursion to sub-directory if there's one
  if (slash != NULL && strlen(++slash) > 0) {
    return locateEntryRelative(slash, entry);
  }

  // return success 
  return isRoot ? 0 : 1;
}

short currentCluster() {
  return (ftell(in) - root - bs.root_dir_entries * sizeof(Fat16Entry)) / (bs.sectors_per_cluster * bs.sector_size) + 2;
}
